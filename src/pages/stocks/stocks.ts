import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpstockProvider} from '../../providers/httpstock/httpstock';

/**
 * Generated class for the StocksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-stocks',
  templateUrl: 'stocks.html',
})
export class StocksPage {

  stocksList = [];

  constructor(public navCtrl: NavController, private stockProvider: HttpstockProvider) {
    this.getActiveS();
  }

  getActiveS(){
    this.stockProvider.getStocks().subscribe(data => this.stocksList = data);


  }

}
