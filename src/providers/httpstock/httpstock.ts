import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';

import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

/*
  Generated class for the HttpstockProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpstockProvider {

  private URL:string = "https://api.iextrading.com/1.0/stock/market/list/mostactive"

  constructor(private http: Http) {
    console.log('Hello HttpstockProvider Provider');
  }

  getStocks(){
    return this.http.get(this.URL)
    .do((res: Response) => console.log(res))
    .map((res: Response) => res.json())
  }

}
